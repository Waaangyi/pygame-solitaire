import random

from Card.card import Card, CardT, CardV
from config import Config


class Game:
    board: list[list[Card]]
    foundation: list[list[Card]]
    stock: list[Card]
    waste: list[Card]

    def __init__(self):
        self.board = [[] for i in range(7)]
        self.foundation = [[] for i in range(4)]
        self.stock = list()
        self.waste = list()

        cards = list()
        types = CardT.__members__.values()
        values = CardV.__members__.values()
        for t in types:
            for v in values:
                cards.append(Card(v, t))
        random.shuffle(cards)
        for i in range(7):
            for j in range(i + 1):
                c = cards.pop()
                c.row = j
                c.column = i
                c.inTableau = True
                c.convert_coords()
                self.board[i].append(c)
            self.board[i][i].show = True

        while len(cards) != 0:
            c = cards.pop()
            c.show = False
            c.inTableau = False
            c.x = Config.horGap
            c.y = Config.stackGap
            c.reset_coords()
            self.stock.append(c)

    def draw_from_stock(self) -> None:
        if len(self.stock) == 0:
            return

        c = self.stock.pop()
        c.x = Config.horGap * 2 + Config.cardWidth
        c.y = Config.stackGap
        c.reset_coords()
        c.show = True
        self.waste.append(c)

    def reset_stock(self) -> None:
        if len(self.stock) != 0:
            return
        while len(self.waste) != 0:
            c = self.waste.pop()
            c.x = Config.horGap
            c.y = Config.stackGap
            c.reset_coords()
            c.show = False
            self.stock.append(c)

    def move_to_stack(self, columnID: int, card: Card) -> None:
        if card.inTableau:
            cards: list[Card] = list()
            row = card.row
            column = card.column
            for i in range(row, len(self.board[column])):
                cards.append(self.board[column][i])
                cards[i - row].row = len(self.board[columnID]) + i - row
                cards[i - row].column = columnID
                cards[i - row].convert_coords()

            while len(self.board[column]) != row:
                self.board[column].pop()

            for c in cards:
                self.board[columnID].append(c)

            if row - 1 >= 0:
                self.board[column][-1].show = True
        else:
            card.row = len(self.board[columnID])
            card.column = columnID
            card.convert_coords()
            card.inTableau = False
            self.board[columnID].append(card)

    def move_to_foundation(self, foundationID: int, card: Card) -> None:
        if card.inTableau:
            self.board[card.column].pop()
            if card.row - 1 >= 0:
                self.board[card.column][-1].show = True
        else:
            self.waste.pop()
        card.x = (Config.horGap + Config.cardWidth) * (3 + foundationID) + Config.horGap
        card.y = Config.stackGap
        card.reset_coords()
        self.foundation[foundationID].append(card)

    def can_stack(self, column: int, card: Card) -> bool:
        if len(self.board[column]) == 0:
            return card.cvalue.value.order == CardV.KING.value.order
        last = self.board[column][-1]
        return last.cvalue.value.order - card.cvalue.value.order == 1 and last.diff_color(card)

    def can_foundation(self, foundationColumn: int, card: Card) -> bool:
        if card.inTableau:
            if card.row != len(self.board[card.column]) - 1:
                return False
        if len(self.foundation[foundationColumn]) == 0:
            return card.cvalue == CardV.ACE
        else:
            return card.ctype == self.foundation[foundationColumn][-1].ctype and \
                   card.cvalue.value.order - self.foundation[foundationColumn][-1].cvalue.value.order == 1


    def check_win(self) -> bool:
        for cards in self.foundation:
            if len(cards) != 13:
                return False
        return True
