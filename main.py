import pygame
from pygame.rect import Rect
from pygame.surface import Surface, SurfaceType
from pygame.time import Clock

from Card.card import Card
from config import Config
from game import Game


class Main:
    screen: Surface | SurfaceType
    clock: Clock
    game: Game
    tableauSelected: Card | None
    wasteSelected: Card | None
    stockRect: Rect
    wasteRect: Rect
    tableauRects: list[Rect]
    foundationRects: list[Rect]
    winNotification: bool
    black: tuple[int, int, int]

    drag: bool

    fromX: int
    fromY: int

    def __init__(self):
        def clone_rectangle(rect: Rect) -> Rect:
            return pygame.rect.Rect(rect.x, rect.y, Config.cardWidth, Config.cardHeight)

        pygame.init()
        self.screen = pygame.display.set_mode([700, 800])
        pygame.display.set_caption("Pygame Solitaire")
        self.screen.fill((0x19, 0x8C, 0x3F))
        self.clock = pygame.time.Clock()
        self.game = Game()
        self.tableauSelected = None
        self.wasteSelected = None
        self.stockRect = clone_rectangle(self.game.stock[0].rect)
        self.wasteRect = pygame.rect.Rect(self.stockRect.x + Config.cardWidth + Config.horGap, self.stockRect.y,
                                          Config.cardWidth, Config.cardHeight)
        self.tableauRects = list()
        self.foundationRects = list()
        self.winNotification = False
        self.black = (0, 0, 0)

        self.drag = False

        self.fromX = -1
        self.fromY = -1

        for i in self.game.board:
            self.tableauRects.append(clone_rectangle(i[0].rect))

        for i in range(4):
            rect = clone_rectangle(self.wasteRect)
            rect.x = (Config.horGap + Config.cardWidth) * (3 + i) + Config.horGap
            self.foundationRects.append(clone_rectangle(rect))

    def run(self) -> None:
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                # mouse clicked event
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    x, y = event.pos
                    if self.stockRect.collidepoint(x, y):
                        if len(self.game.stock) == 0:
                            self.game.reset_stock()
                        else:
                            self.game.draw_from_stock()
                        continue
                    if self.wasteRect.collidepoint(x, y) and len(self.game.waste) != 0:
                        self.wasteSelected = self.game.waste[-1]
                        self.fromX = x - self.wasteRect.x
                        self.fromY = y - self.wasteRect.y
                        self.drag = True
                        continue
                    self.tableauSelected = self.get_board_clicked(x, y)
                    if self.tableauSelected is None or not self.tableauSelected.show:
                        continue
                    self.fromX = x - self.tableauSelected.rect.x
                    self.fromY = y - self.tableauSelected.rect.y
                    self.drag = True

                if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                    x, y = event.pos
                    self.fromX = -1
                    self.fromY = -1
                    processed = False
                    if self.drag and self.tableauSelected is not None:
                        board_drop = self.get_board_drag(x, y)
                        if board_drop != -1 and self.game.can_stack(board_drop, self.tableauSelected):
                            self.game.move_to_stack(board_drop, self.tableauSelected)
                            processed = True
                        foundation_drop = self.get_board_drag_foundation(x, y)
                        if foundation_drop != -1 and self.game.can_foundation(foundation_drop, self.tableauSelected):
                            self.game.move_to_foundation(foundation_drop, self.tableauSelected)
                        if not processed:
                            row = self.tableauSelected.row
                            column = self.tableauSelected.column
                            for i in range(row, len(self.game.board[column])):
                                self.game.board[column][i].reset_coords()
                    if self.drag and self.wasteSelected is not None:
                        board_drop = self.get_board_drag(x, y)
                        if board_drop != -1 and self.game.can_stack(board_drop, self.wasteSelected):
                            self.game.move_to_stack(board_drop, self.wasteSelected)
                            self.game.waste.pop()
                            processed = True
                        foundation_drop = self.get_board_drag_foundation(x, y)
                        if foundation_drop != -1 and self.game.can_foundation(foundation_drop, self.wasteSelected):
                            self.game.move_to_foundation(foundation_drop, self.wasteSelected)
                            processed = True
                        if not processed:
                            self.wasteSelected.reset_coords()

                    self.drag = False
                    self.tableauSelected = None
                    self.wasteSelected = None

                    if self.game.check_win() and not self.winNotification:
                        print("You Won!")
                        self.winNotification = True

                if event.type == pygame.MOUSEMOTION and self.drag:
                    x, y = event.pos
                    if self.tableauSelected is not None:
                        row = self.tableauSelected.row
                        column = self.tableauSelected.column
                        dx = x - (self.tableauSelected.rect.x + self.fromX)
                        dy = y - (self.tableauSelected.rect.y + self.fromY)
                        for i in range(row, len(self.game.board[column])):
                            self.game.board[column][i].rect.x += dx
                            self.game.board[column][i].rect.y += dy
                        continue
                    if self.wasteSelected is not None:
                        dx = x - (self.wasteSelected.rect.x + self.fromX)
                        dy = y - (self.wasteSelected.rect.y + self.fromY)
                        self.wasteSelected.rect.x += dx
                        self.wasteSelected.rect.y += dy
                        continue

            self.render()
            pygame.display.flip()
            if self.drag:
                self.clock.tick(144)
            else:
                self.clock.tick(30)
        pygame.quit()

    def get_board_clicked(self, x: int, y: int) -> Card | None:
        for i in self.game.board:
            for j in reversed(i):
                if j.rect.collidepoint(x, y):
                    return j
        return None

    def get_board_drag(self, x: int, y: int) -> int:
        for i, rect in enumerate(self.tableauRects):
            if rect.collidepoint(x, y):
                return i
        if self.tableauSelected is not None:
            for i in self.game.board:
                for j in i:
                    if j.rect.collidepoint(x, y) and j != self.tableauSelected:
                        return j.column
        if self.wasteSelected is not None:
            for i in self.game.board:
                for j in i:
                    if j.rect.collidepoint(x, y) and j != self.wasteSelected:
                        return j.column
        return -1

    def get_board_drag_foundation(self, x: int, y: int) -> int:
        for i, f in enumerate(self.foundationRects):
            if f.collidepoint(x, y):
                return i
        return -1

    def render(self) -> None:
        self.screen.fill((0x19, 0x8C, 0x3F))

        def draw_rect(rect: Rect) -> None:
            pygame.draw.rect(self.screen, self.black, rect, width=1)

        def draw_card(card: Card) -> None:
            self.screen.blit(card.get_image(), (card.rect.x, card.rect.y))

        # draw the rectangles
        draw_rect(self.stockRect)
        draw_rect(self.wasteRect)
        for i in self.foundationRects:
            draw_rect(i)
        for i in self.tableauRects:
            draw_rect(i)
        # stock and waste
        if len(self.game.stock) != 0:
            draw_card(self.game.stock[-1])
        if len(self.game.waste) != 0:
            for i in range(max(len(self.game.waste) - 2, 0), len(self.game.waste)):
                draw_card(self.game.waste[i])
        # foundation
        for i in self.game.foundation:
            if len(i) != 0:
                draw_card(i[-1])
        for i in self.game.board:
            for j in i:
                draw_card(j)

        # render the moving cards
        if self.tableauSelected is not None:
            row = self.tableauSelected.row
            column = self.tableauSelected.column
            for i in range(row, len(self.game.board[column])):
                draw_card(self.game.board[column][i])
        if self.wasteSelected is not None:
            draw_card(self.wasteSelected)

        if self.winNotification:
            font = pygame.font.Font(pygame.font.get_default_font(), 32)
            text = font.render("You Won!!!", True, (255,0,0), (0,0,0))
            textRect = text.get_rect()
            textRect.center = self.screen.get_rect().center
            self.screen.blit(text, textRect)


if __name__ == "__main__":
    main = Main()
    main.run()
