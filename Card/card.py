from enum import Enum

import pygame
from pygame.rect import Rect
from pygame.surface import Surface, SurfaceType

from config import Config


class EnumElement:

    def __init__(self, name, order):
        self.name = name
        self.order = order

    def __str__(self):
        return self.name


class CardV(Enum):
    ACE = EnumElement("ace", 0)
    TWO = EnumElement("2", 1)
    THREE = EnumElement("3", 2)
    FOUR = EnumElement("4", 3)
    FIVE = EnumElement("5", 4)
    SIX = EnumElement("6", 5)
    SEVEN = EnumElement("7", 6)
    EIGHT = EnumElement("8", 7)
    NINE = EnumElement("9", 8)
    TEN = EnumElement("10", 9)
    JACK = EnumElement("jack", 10)
    QUEEN = EnumElement("queen", 11)
    KING = EnumElement("king", 12)


class CardT(Enum):
    CLUBS = EnumElement("clubs", 0)
    SPADES = EnumElement("spades", 1)
    DIAMONDS = EnumElement("diamonds", 2)
    HEARTS = EnumElement("hearts", 3)


class Card:
    cvalue: CardV
    ctype: CardT
    show: bool
    image: Surface | SurfaceType
    blankImage: Surface | SurfaceType
    rect: Rect
    inTableau: bool
    row: int
    column: int
    x: int
    y: int

    def __init__(self, value: CardV, ctype: CardT):
        self.cvalue = value
        self.ctype = ctype
        self.show = False
        self.image = pygame.image.load("assets/{}_of_{}.png".format(self.cvalue.value, self.ctype.value))
        self.image = pygame.transform.scale(self.image, (Config.cardWidth, Config.cardHeight))
        self.blankImage = pygame.image.load("assets/back.jpg")
        self.blankImage = pygame.transform.scale(self.blankImage, (Config.cardWidth, Config.cardHeight))
        self.rect = pygame.rect.Rect(0,0,Config.cardWidth, Config.cardHeight)
        self.inTableau = False

        self.row = -1
        self.column = -1
        self.x = -1
        self.y = -1

    def get_image(self) -> Surface | SurfaceType:
        return self.image if self.show else self.blankImage

    def convert_coords(self) -> None:
        self.x = Config.horGap + (Config.horGap + Config.cardWidth) * self.column
        self.y = Config.stackGap * self.row + Config.stackGap + Config.cardHeight * 2
        self.reset_coords()

    def reset_coords(self) -> None:
        self.rect.x = self.x
        self.rect.y = self.y

    def compare(self, other) -> None:
        return self.cvalue.value - other.cvalue.cvalue

    def diff_color(self, other) -> bool:
        if self.ctype.value == other.ctype.value:
            return False
        if self.ctype.value.order < 2 and other.ctype.value.order >= 2:
            return True
        return self.ctype.value.order >= 2 and other.ctype.value.order < 2
    def __str__(self):
        return "{} {} {} {} {}".format(self.column, self.row, self.ctype.value, self.cvalue.value, self.show)